
var game = new Phaser.Game(400, 600, Phaser.AUTO, 'canvas');

// add global variable
game.global = {
	userName: 'Anonymous',
	score: 0,
	start: false
}

// add state
game.state.add('prepare', preState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('rank', rankState);

// start state
game.state.start('prepare');
