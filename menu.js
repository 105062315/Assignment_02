/* menuState */

var menuState = {
	menuSelect: 0,
	selectionFram: null,
	
	create: function() {
		game.add.image(0, 0, 'interfaceBackground');
		
		var gameTitle = game.add.text(game.width/2, 200, '小朋友下樓梯', {font: '30px', fill: '#ffffff'});
		gameTitle.anchor.setTo(0.5, 0.5);
		var startButton = game.add.text(game.width/2, 300, 'Start', {font: '30px', fill: '#ffffff'});
		startButton.anchor.setTo(0.5, 0.5);
		var rankingButton = game.add.text(game.width/2, 400, 'Ranking', {font: '30px', fill: '#ffffff'});
		rankingButton.anchor.setTo(0.5, 0.5);
		
		this.selectionFrame = game.add.sprite(game.width/2, 300, 'selectionFrame');
		this.selectionFrame.anchor.setTo(0.5, 0.5);
		
		var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
		upKey.onDown.add(this.pressUP, this);
		var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		downKey.onDown.add(this.pressDOWN, this);
		var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		spaceKey.onDown.add(this.pressSPACE, this);
		
		this.menuSelect = 0;
		game.global.start = false;
	},
	update: function() {
		
	},
	pressUP: function() {
		if(this.menuSelect==0){
			this.menuSelect = 1;
			this.selectionFrame.y = 400;
		}else{
			this.menuSelect -= 1;
			this.selectionFrame.y -= 100;
		}
	},
	pressDOWN: function() {
		if(this.menuSelect==1){
			this.menuSelect = 0;
			this.selectionFrame.y = 300;
		}else{
			this.menuSelect += 1;
			this.selectionFrame.y += 100;
		}
	},
	pressSPACE: function() {
		if(this.menuSelect==0){
			game.state.start('play');
		}else{
			game.state.start('rank');
		}
	}
}