/* loading state */

var loadState = {
	preload: function(){
		game.add.image(0, 0, 'interfaceBackground');
		var loadingText = game.add.text(game.width/2, 200, 'LOADING...', {font: '28px', color: '#FFFFFF'});
		loadingText.anchor.setTo(0.5, 0.5);
		var loadingBar = game.add.sprite(50, game.height/2, 'loadingBar');
		loadingText.anchor.setTo(0.5, 0.5);
		game.load.setPreloadSprite(loadingBar);
		
		/* load game image */
		game.load.image('gameBackground', 'assets/gameBackground.png');
		game.load.image('lifeFrame', 'assets/lifeframe.png');
		game.load.image('lifeBar', 'assets/lifebar.png');
		game.load.image('verticleWall', 'assets/metalwall_verticle.png');
		game.load.image('metalFloor', 'assets/metalfloor.png');
		game.load.image('iceFloor', 'assets/icefloor.png');
		game.load.image('burnedFloor', 'assets/burnedfloor.png');
		game.load.spritesheet('player1', 'assets/player1.png', 26, 45);
		
		/* load game audio*/
		game.load.audio('healSound', ['sound/heal.wav', 'sound/heal.mp3']);
		game.load.audio('damageSound', ['sound/damage.wav', 'sound/damage.mp3']);
		game.load.audio('gameoverSound', ['sound/gameover.wav', 'sound/gameover.mp3']);
		
		/* load menu image */
		game.load.image('selectionFrame', 'assets/menuselectionframe.png');
	},
	create: function() {
		game.state.start('menu');
	}
}