/* playState */

var playState = {
	/* variable declaration*/
	player1: null,				// player
	player1TouchedFloor: false,	// if player first touch the floor
	firstTouch: false,			// if player first touch
	playerSpeed: 150,			// walking speed
	floorSpeed: -100,			// floor lifting speed
	keyboard: null,				// keyboard event
	walls: null,				// group of walls
	floorGroup: null,			// group of floors
	metalFloors: null,			// array of metal floors
	burnedFloors: null,			// array of burned floors
	iceFloors: null,			// array of ice floors
	upperWall: null,			// the toppest burned wall
	scoreLabel: null,			// label to show current floor
	lifeBar: null,				// dynamic show current life
	healSound: null,			// heal sound effect
	damageSound: null,			// damage sound effect
	gameoverSound: null,		// gameover sound effect
	isPause: false,				// if the game is paused
	isEnd: false,				// if the game is ended
	
	/* phaser function */
	create: function(){
		/* set interface */
		game.add.image(0, 0, 'interfaceBackground');
		game.add.image(0, 50, 'gameBackground');
		game.add.text(10, 10, 'LIFE', {size: '16px', fill: '#FF0000'});
		game.add.image(75, 10, 'lifeFrame');
		this.scoreLabel = game.add.text(300, 30, 'Floor: 0', {size: '20px', fill: '#FFFFFF'});
		this.scoreLabel.anchor.setTo(0.5, 0.5);
		
		// set sound effect
		this.healSound = game.add.audio('healSound');
		this.damageSound = game.add.audio('damageSound');
		this.gameoverSound = game.add.audio('gameoverSound');
		
		// set player1
		this.player1 = game.add.sprite(200, 450, 'player1');
		this.player1.anchor.setTo(0.5, 0.5);
		this.player1.animations.add('standstill', [0], 5, true);
		this.player1.animations.add('walkleft', [1, 2], 5, true);
		this.player1.animations.add('walkright', [3, 4], 5, true);
		game.physics.arcade.enable(this.player1);
		this.player1.health = 10;
		this.player1.maxHealth = 10;
		this.player1.body.gravity.y = 600;
		
		// set life bar
		this.lifeBar = game.add.sprite(75, 10, 'lifeBar');
		this.lifeBar.width = 100;
		
		// set burned floors
		this.burnedFloors = [];
		for(i=0; i<3; i=i+1){
			this.burnedFloors.push(game.add.sprite(0, 0, 'burnedFloor'));
		}
		// set ice floors
		this.iceFloors = [];
		for(i=0; i<2; i=i+1){
			this.iceFloors.push(game.add.sprite(0, 0, 'iceFloor'));
		}
		// set normal floors
		this.metalFloors = [];
		for(i=0; i<10; i=i+1){
			this.metalFloors.push(game.add.sprite(0, 0, 'metalFloor'));
		}
		// set floor group
		this.floorGroup = game.add.group();
		this.floorGroup.enableBody = true;
		this.floorGroup.addMultiple(this.burnedFloors);
		this.floorGroup.addMultiple(this.iceFloors);
		this.floorGroup.addMultiple(this.metalFloors);
		this.floorGroup.setAll('body.immovable', true);
		// generate 
		var floor = this.floorGroup.sendToBack(this.floorGroup.getTop());
		floor.reset(150, 500);
		floor.body.velocity.y = this.floorSpeed;
		
		// generate floors
		game.time.events.loop(750, this.generateFloor, this);
		
		// set wall
		this.walls = game.add.group();
		this.walls.enableBody = true;
		this.upperWall = game.add.sprite(0, 50, 'burnedFloor', 0, this.walls);
		this.upperWall.width = 400;
		game.add.sprite(0, 50, 'verticleWall', 0, this.walls);
		game.add.sprite(380, 50, 'verticleWall', 0, this.walls);
		this.walls.setAll('body.immovable', true);
		
		// get keyboard element
		this.keyboard = game.input.keyboard.addKeys({
			'up': Phaser.KeyCode.UP,
			'down': Phaser.KeyCode.DOWN,
			'left': Phaser.KeyCode.LEFT,
			'right': Phaser.KeyCode.RIGHT,
			'space': Phaser.KeyCode.SPACEBAR,
			'esc': Phaser.KeyCode.ESC
		});
		this.keyboard.esc.onDown.add(this.pressESC, this);
		this.keyboard.space.onDown.add(this.pressSpace, this);
		
		// initialize variable
		game.global.start = true;
		game.global.score = 0;
		this.isEnd = false;
		this.isPause = false;
		game.time.events.loop(5000, this.updateScore, this);
	},
	update: function(){
		game.physics.arcade.collide(this.player1, this.metalFloors, this.stepMetalFloors);
		game.physics.arcade.collide(this.player1, this.burnedFloors, this.stepBurnedFloors);
		game.physics.arcade.collide(this.player1, this.iceFloors, this.stepIceFloors);
		game.physics.arcade.collide(this.player1, this.upperWall, this.collideUpperWall);
		game.physics.arcade.collide(this.player1, this.walls);
		
		this.keyboardDetect();
		this.playerDetect();
		this.recycleFloor();
	},
	
	/* customerized function */
	keyboardDetect: function() {
		// player movement
		if(!this.isPause){
			if(this.keyboard.left.isDown){
				this.player1.body.velocity.x = - this.playerSpeed;
				this.player1.animations.play('walkleft');
			}else if(this.keyboard.right.isDown){
				this.player1.body.velocity.x = this.playerSpeed;
				this.player1.animations.play('walkright');
			}else{
				this.player1.body.velocity.x = 0;
				this.player1.animations.stop();
				this.player1.frame = 0;
			}
		}
	},
	pressESC: function(){
		// pause/resume game
		if(this.isPause)
			this.resumeGame();
		else
			this.pauseGame();
	},
	pressSpace: function() {
		if(this.isEnd){
			game.state.start('rank');
		}
	},
	playerDetect: function(){
		if(!this.isPause){
			if(!this.player1.body.touching.down){
				// if player is on air
				this.player1TouchedFloor = false;
				this.playerSpeed = 150;
			}
			if(!this.player1.inWorld){
				this.endGame();
			}
		}
	},
	generateFloor: function(){
		if(!this.isPause){
			// randomly pick one floor from group
			var deadNumber = this.floorGroup.countDead();
			var floor = this.floorGroup.sendToBack(this.floorGroup.getRandom(this.floorGroup.length-deadNumber, deadNumber));
			if(!floor)
				return;
			// randomly set the position of the floor
			floor.reset(game.rnd.between(0, 300), 600);
			floor.body.velocity.y = this.floorSpeed;
		}
	},
	recycleFloor: function(){
		this.floorGroup.forEachAlive(this.checkInWorld, this);
	},
	checkInWorld: function(floor){
		if(floor.y<=50)
			floor.kill();
	},
	stepMetalFloors: function(){
		if(!playState.player1TouchedFloor && playState.player1.body.touching.down){
			playState.healSound.play();
			if(playState.player1.health<10){
				playState.player1.heal(1);
				playState.lifeBar.width = 10 * playState.player1.health;
			}
			playState.player1TouchedFloor = true;
		}
	},
	stepBurnedFloors: function() {
		if(!playState.player1TouchedFloor && playState.player1.body.touching.down){
			if(playState.player1.health>4){
				playState.damageSound.play();
				playState.player1.damage(4);
			}else{
				playState.player1.health = 0;
				playState.endGame();
			}
			playState.lifeBar.width = 10 * playState.player1.health;
			playState.player1TouchedFloor = true;
		}
	},
	stepIceFloors: function() {
		if(!playState.player1TouchedFloor && playState.player1.body.touching.down){
			playState.playerSpeed = 250;
			playState.player1TouchedFloor = true;
		}
	},
	collideUpperWall: function() {
		if(!playState.firstTouch){
			if(playState.player1.health>4){
				playState.damageSound.play();
				playState.player1.damage(4);
			}else{
				playState.player1.health = 0;
				playState.endGame();
			}
			playState.lifeBar.width = 10 * playState.player1.health;
			playState.firstTouch = true;
			game.time.events.add(500, function(){playState.firstTouch = false;}, this);
		}
	},
	updateScore: function(){
		if(!this.isPause){
			game.global.score += 1;
			this.scoreLabel.text = "Floor: " + game.global.score;
			game.add.tween(this.scoreLabel.scale).to({x: 1.3, y: 1.3}, 100).yoyo(true).start();
			
			if(game.global.score%5 == 0){
				this.floorSpeed -= 5;
				this.floorGroup.setAll('body.velocity.y', this.floorSpeed);
			}
		}
	},
	resumeGame: function(){
		this.isPause = false;
		this.floorGroup.setAll('body.velocity.y', this.floorSpeed);
		this.player1.body.gravity.y = 400;
	},
	pauseGame: function() {
		this.isPause = true;
		this.floorGroup.setAll('body.velocity.y', 0);
		this.player1.body.gravity.y = 0;
		this.player1.body.velocity.x = 0;
		this.player1.body.velocity.y = 0;
	},
	endGame: function() {
		this.gameoverSound.play();
		this.pauseGame();
		this.isEnd = true;
		var text = game.add.text(game.width/2, 500, 'press SPACE to end', {size: '24px', fill: '#FFFFFF'});
		text.anchor.setTo(0.5, 0.5);
		game.add.tween(text.scale).to({x:1.3, y:1.3}, 1000).yoyo(true).loop().start();
	}
}