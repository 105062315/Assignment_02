/* ranking */

var rankState = {
	/* variable */
	updateList: false,			// is rank list updated
	
	create: function() {
		// set interface
		game.add.image(0, 0, 'interfaceBackground');
		var text = game.add.text(game.width/2, 80, 'Your Score', {font: '20px', fill: '#FFFFFF'});
		text.anchor.setTo(0.5, 0.5);
		text = game.add.text(game.width/2, 120, game.global.score + ' Floors', {font: '32px', fill: '#FFFFFF'});
		text.anchor.setTo(0.5, 0.5);
		text = game.add.text(game.width/2, 200, 'Ranking: ', {font: '20px', fill: '#FFFFFF'});
		text.anchor.setTo(0.5, 0.5);
		for(i=1; i<=10; i++)
			game.add.text(100, 200+i*30, 'No. ' + i, {font: '16px', fill: '#FFFFFF'});
		game.add.text(game.width/2, 570, 'Press SPACE back to menu', {font: '16px', fill: '#AAAAAA'}).anchor.setTo(0.5, 0.5);
		
		// reset parameters
		this.updateList = false;
		game.global.userName = document.querySelector('#userName').value;
		
		// load ranking list
		firebase.database().ref('NS-SHAFT').once('value')
			.then(function(snapshot) {
				console.log('Load ranking list successfully.');
				rankState.rankScore(snapshot.val());
			})
			.catch(function(e) {
				console.log('Failed to load ranking list.');
				console.log('Message: ' + e.message);
			});
		
		// go back to menu
		game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(function(){
			game.state.start('menu');
		});
		game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(function(){
			game.state.start('menu');
		});
	},
	/* customized function */
	rankScore: function(scoreData) {
		var rankingList = {};
		var text;
		var isRanked =  game.global.start ? false : true;
		var count = 1;
		for(rank in scoreData){
			if((scoreData[rank].score <= game.global.score) && !isRanked){
				rankingList[String(count)] = {
					name: game.global.userName,
					score: game.global.score
				};
				text = game.add.text(160, 200+30*count, game.global.userName + ': ' + game.global.score + ' floors', {font: '16px', fill: '#FF0000'});
				rankState.updateList = true;
				isRanked = true;
				count = count + 1;
				if(count>10)
					break;
			}
			rankingList[String(count)] = {
				name: scoreData[rank].name,
				score: scoreData[rank].score
			};
			text = game.add.text(160, 200+30*count, scoreData[rank].name + ': ' + scoreData[rank].score + ' floors', {font: '16px', fill: '#FFFFFF'});
			count = count + 1;
			if(count>10)
				break;
		}
		if(count<10 && !isRanked){
			rankingList[String(count)] = {
				name: game.global.userName,
				score: game.global.score
			};
			text = game.add.text(160, 200+30*count, game.global.userName + ': ' + game.global.score + ' floors', {font: '16px', fill: '#FF0000'});
			rankState.updateList = true;
			count = count + 1;
		}
		for( ; count<=10; count++){
			text = game.add.text(160, 200+30*count, 'empty', {font: '16px', fill: '#AAAAAA'});
		}
		if(rankState.updateList){
			firebase.database().ref('NS-SHAFT').update(rankingList);
		}
	}
}