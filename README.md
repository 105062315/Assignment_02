# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## Report

  **作品網址：[小朋友下樓梯](https://105062315.gitlab.io/Assignment_02)。**

### Basic functions
  **摘要** 

|                           functon							            	| Complete | 
|:-----------------------------------------------------------:|:--------:|
| Complete game process							                  				| 	 v	   |
| Follow rules of "小朋友下樓梯"                                | 	 v	   |
| All things have correct physical properties and behaviors.	| 	 v	   |
| special mechanisms									                    		| 	 v	   |
| sound effects and UI								                  			| 	 v	   |
| leaderboard with firebase							                			| 	 v	   |

  **詳細**
  1. game process:   
   * menu => game => rank => menu
   * menu => rank => menu
  2. special mechanisms:   
   * 紅色地板會扣血
   * 在結冰地板上移動會加速
  3. sound effect and UI:   
   * 加/扣血時有對應音效
   * 角色死亡時有死亡音效
   * 增加分數時樓層數會有醒目放大動畫
  4. leaderboard:   
   <img src="reportImg/rankingList.PNG" height="500px"/>
   
### Advanced functions
  1. 遊戲中可由Esc暫停/繼續遊戲。
  2. 隨分數增加樓梯上升速度。
  3. 可直接在menu進入排行榜頁面。
  4. 排行榜中若有上榜則紅字突出玩家成績。

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 
## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
