/* prepare state */

var preState = {
	preload: function() {
		game.load.image('loadingBar', 'assets/loadingBar.png');
		game.load.image('interfaceBackground', 'assets/background.png');
	},
	create: function() {
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.state.start('load');
	}
};